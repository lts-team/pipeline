# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.
---
workflow:
  rules:
    # Avoid duplicated pipelines, do not run detached pipelines.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:
        IMAGE_VERSION: latest
        AUTOPKGTEST_IMAGE_VERSION: latest
        SID_VERSION: latest
        RELEASE_VERSION: ${RELEASE}
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      variables:
        IMAGE_VERSION: ${RELEASE}_${CI_COMMIT_REF_NAME}
        AUTOPKGTEST_IMAGE_VERSION: ${STABLE}_${CI_COMMIT_REF_NAME}
        SID_VERSION: sid_${CI_COMMIT_REF_NAME}
        RELEASE_VERSION: ${RELEASE}_${CI_COMMIT_REF_NAME}
    - when: always

variables:
  DOCKER_TLS_CERTDIR: ""
  SALSA_CI_DISABLE_APTLY: "false"
  SALSA_CI_PERSIST_IMAGES:
    description: "Images are clean at the end of a pipeline of a staging branch. Set this to 1 to don't clean them"
    value: 0
  SALSA_CI_DISABLE_BUILD_IMAGES:
    description: "Set this to 1 if you want to disable the jobs that build the images"
    value: 0
  BUILD_ALL_IMAGES:
    description: "Set this to 1 if you want to build the full set of images, even in staging branches"
    value: 0
  SALSA_CI_ARM_RUNNER_TAG: "arm64"
  # Used by .images-debian.yml to define the base for the
  # autopkgtest image
  STABLE: "bookworm"
  SALSA_CI_TEAM_IMAGES: 'registry.salsa.debian.org/salsa-ci-team/pipeline'
  FF_SCRIPT_SECTIONS: "true" # enables full command logging in job logs

stages:
  - images
  - test
  - clean

images:
  stage: images
  trigger:
    include:
      - local: .images-elts.yml
    strategy: depend
    # https://gitlab.com/gitlab-org/gitlab/-/issues/341508#note_1939725601
    forward:
      pipeline_variables: true
  rules:
    - if: $SALSA_CI_DISABLE_BUILD_IMAGES !~ /^(1|yes|true)$/
      when: always

check license + contributor:
  stage: test
  image: ${SALSA_CI_TEAM_IMAGES}/base:latest
  dependencies: []
  variables:
    RELEASE: sid
  before_script:
    - apt-get update && apt-get upgrade -y
    - apt-get install -y licensecheck
  script:
    - PROBLEMS=0
    # check files without any license
    - |
      BAD_FILES=$(licensecheck -r . | \
      grep -Ev '(images/files)|(images/patches)|(README.md)|(CONTRIBUTING.md)|(CONTRIBUTORS)|(STRUCTURE.md)'| \
      grep UNKNOWN) || true
    - |
      [ -z "$BAD_FILES" ] || \
      (echo "ERROR: Missing license statement in the following files:"; \
      echo "$BAD_FILES"; exit 1) || \
      PROBLEMS=$(($PROBLEMS + 1))
    # check email or name is in the list of contributors
    - |
      grep -q "${GITLAB_USER_EMAIL}" CONTRIBUTORS || \
      grep -q "${GITLAB_USER_NAME}" CONTRIBUTORS || \
      (echo "ERROR: ${GITLAB_USER_NAME} <${GITLAB_USER_EMAIL}> missing in the CONTRIBUTORS file"; exit 1) || \
      PROBLEMS=$(($PROBLEMS + 1))
    - exit $PROBLEMS
  rules:
    - if: $CI_SERVER_URL != "https://salsa.debian.org"
      when: never

check gpg signature:
  stage: test
  image: ${SALSA_CI_TEAM_IMAGES}/base:latest
  dependencies: []
  variables:
    RELEASE: sid
  before_script:
    - apt-get update && apt-get upgrade -y
    - apt-get install -y --no-install-recommends debian-keyring dirmngr git gpg gpg-agent ca-certificates
  script:
    - mkdir -p ${HOME}/.gnupg
    - chmod 600 ${HOME}/.gnupg
    - echo "keyring /usr/share/keyrings/debian-keyring.gpg" >> ${HOME}/.gnupg/gpg.conf
    - echo "keyring /usr/share/keyrings/debian-maintainers.gpg" >> ${HOME}/.gnupg/gpg.conf
    - echo "keyring /usr/share/keyrings/debian-nonupload.gpg" >> ${HOME}/.gnupg/gpg.conf
    - echo "Checking that contributors have signed git commits following to CONTRIBUTING.md .."
    # The field %GK is the only one that will have a key ID if before the key has been downloaded
    - SIGNING_KEY="$(git log --pretty="%GK" HEAD~1..HEAD)"
    - 'if [ -z "$SIGNING_KEY" ]; then echo "ERROR: Git commit message signature missing!" && exit 1; fi'
    # Try OpenPGP first and if key was not found, try the Ubuntu keyserver.
    # Defining the keyserver for OpenPGP is done to be explicit, but it not
    # strictly necessary as is it is the default keyserver in GnuPG anyway
    - >
      (gpg --verbose --keyserver hkps://keys.openpgp.org:443 --recv-key $SIGNING_KEY ||
      gpg --verbose --keyserver hkps://keyserver.ubuntu.com:443 --recv-key $SIGNING_KEY) ||
      (echo "ERROR: Key not found. Please upload it to the OpenPGP or Ubuntu keyserver." && exit 1)
    - >
      GIT_TRACE2=true git verify-commit HEAD --raw --verbose ||
      (echo "ERROR: Signature is invalid. Please sign your commits correctly. See CONTRIBUTING.md for details." && exit 1)
  rules:
    - if: $CI_SERVER_URL != "https://salsa.debian.org"
      when: never
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

check yaml lint:
  stage: test
  image: ${SALSA_CI_TEAM_IMAGES}/base:latest
  dependencies: []
  variables:
    RELEASE: sid
  before_script:
    - apt-get update && apt-get upgrade -y
    - apt-get install -y yamllint
  script:
    - yamllint .

test-pipeline:
  variables:
    RELEASE: sid
  trigger:
    include:
      - local: .pipeline-test.yml
    strategy: depend
  parallel:
    matrix:
      - PROJECT_URL: https://salsa.debian.org/debian/grep
        PROJECT_TARGET_HEAD: debian/3.6-1
        RELEASE: bullseye
        # crossbuild is only supported for unstable
        SALSA_CI_DISABLE_CROSSBUILD_ARM64: 1
        # enable armhf and arm64 builds
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARMHF: 0
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARM64: 0
      - PROJECT_URL: https://salsa.debian.org/debian/grep
        PROJECT_TARGET_HEAD: debian/3.3-1
        RELEASE: buster
        # crossbuild is only supported for unstable
        SALSA_CI_DISABLE_CROSSBUILD_ARM64: 1
        # enable armhf and arm64 builds
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARMHF: 0
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARM64: 0
      - PROJECT_URL: https://salsa.debian.org/debian/grep
        PROJECT_TARGET_HEAD: debian/2.27-2
        RELEASE: stretch
        VENDOR: 'freexian'
        SALSA_CI_MIRROR: 'http://deb.freexian.com/extended-lts'
        SALSA_CI_COMPONENTS: 'main contrib non-free'
        SALSA_CI_PIUPARTS_ARGS: '--defaults debian --keyring /etc/apt/trusted.gpg.d/freexian-archive-extended-lts.gpg'
        # enable armhf builds
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARMHF: 0
        # crossbuild is only supported for unstable
        SALSA_CI_DISABLE_CROSSBUILD_ARM64: 1
      - PROJECT_URL: https://salsa.debian.org/debian/grep
        PROJECT_TARGET_HEAD: debian/2.20-4
        RELEASE: jessie
        VENDOR: 'freexian'
        SALSA_CI_MIRROR: 'http://deb.freexian.com/extended-lts'
        SALSA_CI_COMPONENTS: 'main contrib non-free'
        SALSA_CI_PIUPARTS_ARGS: '--defaults debian --keyring /etc/apt/trusted.gpg.d/freexian-archive-extended-lts.gpg'
        # reprotest is not available in jessie
        SALSA_CI_DISABLE_REPROTEST: 1
        # enable armel and armhf builds
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARMEL: 0
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARMHF: 0
        # crossbuild is only supported for unstable
        SALSA_CI_DISABLE_CROSSBUILD_ARM64: 1
      - PROJECT_URL: https://salsa.debian.org/debian/grep
        PROJECT_TARGET_HEAD: debian/2.20-4
        RELEASE: jessie
        VENDOR: 'freexian'
        SALSA_CI_MIRROR: 'http://deb.freexian.com/extended-lts'
        SALSA_CI_COMPONENTS: 'main contrib non-free'
        # reprotest is not available in jessie
        SALSA_CI_DISABLE_REPROTEST: 1
        # enable armel and armhf builds
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARMEL: 0
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARMHF: 0
        # crossbuild is only supported for unstable
        SALSA_CI_DISABLE_CROSSBUILD_ARM64: 1
        SALSA_CI_DISABLE_AUTOPKGTEST: 1
        SALSA_CI_DISABLE_PIUPARTS: 1

# test pipeline to test the build jobs in architectures non-enabled-by-default
# While we don't have shared runners for these architectures, this pipeline
# has to be manually triggered in Salsa CI/pipeline forks.
test-pipeline-non-default-archs:
  extends: test-pipeline
  trigger:
    include:
      - local: .pipeline-test.yml
    strategy: depend
  rules:
    - if: $CI_SERVER_URL == "https://salsa.debian.org" && $CI_PROJECT_PATH == "salsa-ci-team/pipeline"
      when: always
    - if: $SALSA_CI_TEST_NON_DEFAULT_ARCHS =~ /^(1|yes|true)$/
      when: always
    - when: manual
  parallel:
    matrix:
      - PROJECT_URL: https://salsa.debian.org/debian/grep
        PROJECT_TARGET_HEAD: debian/master
        SALSA_CI_DISABLE_ALL_TESTS: 1
        SALSA_CI_DISABLE_APTLY: 1
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARM64: 0
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARMEL: 0
        SALSA_CI_DISABLE_BUILD_PACKAGE_ARMHF: 0
  allow_failure: true

clean images:
  stage: clean
  image: docker:latest
  services:
    - docker:20.10.12-dind
  variables:
    IMAGES_LIST: 'aptly autopkgtest base arm32v5/base arm32v7/base arm64v8/base i386/base blhc gbp piuparts reprotest lintian generic_tests'
    RELEASE: sid
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - wget -O /usr/bin/reg https://github.com/genuinetools/reg/releases/download/v0.13.0/reg-linux-amd64
    - chmod +x /usr/bin/reg
  script:
    - |
      # If not master, use REF as staging tag.
      if ! [ "${CI_COMMIT_REF_NAME}" = "master" ]; then
        STAGING_TAG="_${CI_COMMIT_REF_NAME}"
      fi
    - |
      for IMAGE in $IMAGES_LIST; do
        reg --registry ${CI_REGISTRY} rm ${CI_PROJECT_PATH}/${IMAGE}:${RELEASE}${STAGING_TAG} || true
      done
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH && $SALSA_CI_PERSIST_IMAGES !~ /^(1|yes|true)$/
      when: always
