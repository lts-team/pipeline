# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.
---
include:
  - .images-ci.yml

.elts-releases: &elts-releases
  - jessie
  - stretch
  - buster

# Images built on main branch
images-elts-prod:
  stage: build
  extends: .build_template
  parallel:
    matrix:
      # All releases, all arches
      - IMAGE_NAME:
          - base
        VENDOR: freexian
        ARCH:
          - i386
          - amd64
        RELEASE: *elts-releases
      # All releases, only amd64
      - IMAGE_NAME:
          - blhc
          - generic_tests
          - lintian
        VENDOR: freexian
        ARCH: amd64
        RELEASE: *elts-releases
      # reprotest not available on jessie
      - IMAGE_NAME:
          - reprotest
        VENDOR: freexian
        ARCH: amd64
        RELEASE: stretch
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
    - if: $BUILD_ALL_IMAGES

# Images for ARM. We list them here because we need a separate tag
# for the CI runner
images-elts-prod-arm:
  stage: build
  extends: .build_template
  tags:
    - arm64
  parallel:
    matrix:
      # All releases, all arches
      - IMAGE_NAME:
          - base
        VENDOR: freexian
        ARCH:
          - arm32v7
        RELEASE: *elts-releases
      - IMAGE_NAME:
          - base
        VENDOR: freexian
        ARCH:
          - arm32v5
        RELEASE:
          - jessie
      - IMAGE_NAME:
          - base
        VENDOR: freexian
        ARCH:
          - arm64v8
        RELEASE:
          - buster
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
    - if: $BUILD_ALL_IMAGES

# Images built on branches.
# This is a subset of all the images, and are the only ones
# built for branches != main.
images-elts-staging:
  extends: .build_template
  stage: build
  parallel:
    matrix:
      # All releases, all arches
      - IMAGE_NAME:
          - base
        VENDOR: freexian
        ARCH:
          - i386
          - amd64
        RELEASE: *elts-releases
      # All releases, only amd64
      - IMAGE_NAME:
          - blhc
          - generic_tests
          - lintian
        VENDOR: freexian
        ARCH: amd64
        RELEASE: *elts-releases
      # reprotest not available on jessie
      - IMAGE_NAME:
          - reprotest
        VENDOR: freexian
        ARCH: amd64
        RELEASE: stretch
  rules:
    - if: $BUILD_ALL_IMAGES
      when: never
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

# Images for ARM. We list them here because we need a separate tag
# for the CI runner
images-elts-staging-arm:
  extends: .build_template
  stage: build
  tags:
    - arm64
  parallel:
    matrix:
      # All releases, all arches
      - IMAGE_NAME:
          - base
        VENDOR: freexian
        ARCH:
          - arm32v7
        RELEASE: *elts-releases
      - IMAGE_NAME:
          - base
        VENDOR: freexian
        ARCH:
          - arm32v5
        RELEASE:
          - jessie
      - IMAGE_NAME:
          - base
        VENDOR: freexian
        ARCH:
          - arm64v8
        RELEASE:
          - buster
  # While there isn't an ARM shared runner available, let's allow these images
  # to fail to not block pipelines on MRs.
  allow_failure: true
  rules:
    - if: $BUILD_ALL_IMAGES
      when: never
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
